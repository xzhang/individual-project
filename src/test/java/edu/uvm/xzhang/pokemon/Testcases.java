package edu.uvm.xzhang.pokemon;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class Testcases {
    private static Player player1;

    @BeforeClass
    public static void setUp() {
        player1 = new Player("xzhang1", 1);
    }

    @After
    public void show() {
        System.out.println("Player 1 infos:");
        System.out.println("Id: " + player1.getId());
        System.out.println("Name: " + player1.getName());
        System.out.println("Bag: " + player1.getBag());
        System.out.println("Box: " + player1.getBox());

        player1.clearAll();
        player1.setName("xzhang1");
        player1.setId(1);
        System.out.println("\n\n\n");
    }

    // Test addPokemon function, getBag and getBox functions by calling the getSize in Bag and Box classes
    @Test
    public void TestOne() {
        player1.addPokemon(new Pokemon("Pokemon 1", 1));
        Assert.assertEquals(player1.getBag().getSize(), 1);
        Assert.assertEquals(player1.getBox().getSize(), 0);
    }

    // Test exchangePokemonFromBagToBox and exchangePokemonFromBoxToBag functions
    @Test
    public void TestTwo() {
        Assert.assertEquals(player1.getBag().getSize(), 0); // test clearAll function in Player class
        Assert.assertEquals(player1.getBox().getSize(), 0);

        Pokemon pokemon1 = new Pokemon("Pokemon 1", 1);
        player1.addPokemon(pokemon1);
        player1.addPokemon(new Pokemon("Pokemon 2", 2));
//        System.out.println("Bag:--- "+player1.getBag());
//        System.out.println("Box:--- "+player1.getBox());
        Assert.assertEquals(player1.getBag().getSize(), 2);
        Assert.assertEquals(player1.getBox().getSize(), 0);

        player1.exchangePokemonFromBagToBox(pokemon1);
//        System.out.println("Bag:--- "+player1.getBag());
//        System.out.println("Box:--- "+player1.getBox());
        Assert.assertEquals(player1.getBag().getSize(), 1);
        Assert.assertEquals(player1.getBox().getSize(), 1);


        player1.exchangePokemonFromBoxToBag(new Pokemon("Pokemon 1", 1));
//        System.out.println("Bag:--- "+player1.getBag());
//        System.out.println("Box:--- "+player1.getBox());
        Assert.assertEquals(player1.getBag().getSize(), 2);
        Assert.assertEquals(player1.getBox().getSize(), 0);
    }

    // Test if addPokemon will automatically add the Pokemon(s) to the box when the bag is full
    @Test
    public void TestThree() {
        for (int i = 0; i < 10; i++) {
            player1.addPokemon(new Pokemon("Pokemon " + i, i));
        }
//        System.out.println("Bag:--- "+player1.getBag());
//        System.out.println("Box:--- "+player1.getBox());
        Assert.assertEquals(player1.getBag().getSize(), 6);
        Assert.assertEquals(player1.getBox().getSize(), 4);
    }

    // Test getPokemon by id or name functions from bag and box
    @Test
    public void testFour() {
        Pokemon pokemon1 = new Pokemon("Pokemon 1", 1);
        player1.addPokemon(pokemon1);
        player1.addPokemon(new Pokemon("Pokemon 2", 2));

        Assert.assertEquals(player1.getPokemonFromBag(1), pokemon1);
        Assert.assertEquals(player1.getPokemonFromBag("Pokemon 2").get(0), new Pokemon("Pokemon 2", 2));

        Pokemon pokemon2 = new Pokemon("Pokemon 2", 2);
        player1.exchangePokemonFromBagToBox(pokemon1);
        player1.exchangePokemonFromBagToBox(pokemon2);

        Assert.assertEquals(player1.getPokemonFromBox(1), pokemon1);
        Assert.assertEquals(player1.getPokemonFromBox("Pokemon 2").get(0), new Pokemon("Pokemon 2", 2));
    }

    // Test setId function for Player class and check if ids in bag and box will be updated.
    @Test
    public void testFive() {
        player1.setName("xzhang2");
        player1.setId(2);

        Assert.assertEquals(player1.getName(), "xzhang2");
        Assert.assertEquals(player1.getId(), 2);
        Assert.assertEquals(player1.getBag().getPlayerId(), 2);
        Assert.assertEquals(player1.getBox().getPlayerId(), 2);
    }


    // Test incorrectSetId function for Player class.
    // It is incorrect because the player only update the id in Player objects without updating playerId in
    // Bag and Box objects.
    // But I only check the updated id in Player object
    // and did NOT use assert to check if bag and box objects have updated
    // playerId.
    @Test
    public void testSixIncorrect() {
        player1.setName("xzhang2");
        player1.incorrectSetId(2);

        Assert.assertEquals(player1.getName(), "xzhang2");
        Assert.assertEquals(player1.getId(), 2);


        // The correct way is to add these two lines;
//        Assert.assertEquals(player1.getBag().getPlayerId(), 2);
//        Assert.assertEquals(player1.getBox().getPlayerId(), 2);
    }
}
