package edu.uvm.xzhang.pokemon;
import java.util.Objects;
import java.util.Scanner;


public class Main {
    public static void main(String[] args){
        String choice = "";
        Scanner scan = new Scanner(System.in);
        while(true){
            System.out.println("Enter 1 for continue, others for end: ");
            if (scan.hasNext()) {
                choice = scan.next();
            }
            if(choice.equals("1")){
                System.out.println("Enter your name: ");
                String name = "new Player";
                if (scan.hasNext()) {
                    name = scan.next();
                }
                System.out.println("Enter your id(number): ");
                int id = 0;
                if (scan.hasNextInt()) {
                    String strId = scan.next();
                    id = Integer.parseInt(strId);
                }
                Player player = new Player(name, id);
                System.out.println("Enter how many Pokemons you want to add: ");
                int num = 0;
                if (scan.hasNext()) {
                    String strNum = scan.next();
                    num = Integer.parseInt(strNum);
                }
                if(num > 0){
                    for(int i = 0; i<num;i++){
                        System.out.println("Enter the No." + (i+1) + " Pokemon's Info you want to add: ");
                        System.out.println("Pokemon Name: ");
                        String namePokemon = "new Pokemon";
                        if (scan.hasNext()) {
                            namePokemon = scan.next();
                        }
                        System.out.println("Pokemon id(number): ");
                        int idPokemon = 0;
                        if (scan.hasNextInt()) {
                            String strId = scan.next();
                            idPokemon = Integer.parseInt(strId);
                        }
                        player.addPokemon(new Pokemon(namePokemon, idPokemon));
                        System.out.println("\n");
                    }

                    System.out.println("Do you want to move any pokemon from bag/box to box/bag?: ");
                    System.out.println("1 for from bag to box, 2 for from box to bag, any other number for skip: ");
                    int answer = 0;
                    if (scan.hasNext()) {
                        String strNum = scan.next();
                        answer = Integer.parseInt(strNum);
                    }
                    while (answer != 0){
                        System.out.println("\n");
                        System.out.println("This is your bag:\n"+player.getBag()+"\n");
                        System.out.println("This is your box:\n"+player.getBox()+"\n");
                        System.out.println("Enter the Pokemon's Info you want to exchange: ");
                        System.out.println("Pokemon Name: ");
                        String namePokemon = "new Pokemon";
                        if (scan.hasNext()) {
                            namePokemon = scan.next();
                        }
                        System.out.println("Pokemon id(number): ");
                        int idPokemon = 0;
                        if (scan.hasNextInt()) {
                            String strId = scan.next();
                            idPokemon = Integer.parseInt(strId);
                        }
                        switch (answer){
                            case 1:
                                player.exchangePokemonFromBagToBox(new Pokemon(namePokemon, idPokemon));
                                System.out.println("Finished exchanging!");
                                break;
                            case 2:
                                player.exchangePokemonFromBoxToBag(new Pokemon(namePokemon, idPokemon));
                                System.out.println("Finished exchanging!");
                                break;
                            default:
                        }
                        System.out.println("\n");
                        System.out.println("This is your bag:\n"+player.getBag()+"\n");
                        System.out.println("This is your box:\n"+player.getBox()+"\n");
                    }

                }
                System.out.println("\nDo you want to check your bag or box again?(y for yes, others for no)");
                String answerCheck = "";
                if (scan.hasNext()) {
                    answerCheck= scan.next();
                }
                if(answerCheck.equals("y")){
                    System.out.println("\n");
                    System.out.println("This is your bag:\n"+player.getBag()+"\n");
                    System.out.println("This is your box:\n"+player.getBox()+"\n");
                }
                System.out.println("Finished this player...");
            }else{
                scan.close();
                break;
            }
        }
        System.out.println("Program exit...");
    }
}
