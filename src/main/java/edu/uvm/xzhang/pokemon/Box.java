package edu.uvm.xzhang.pokemon;

import java.util.ArrayList;

public class Box {
    // instance variables
    private int playerId;
    private ArrayList<Pokemon> pokemons;

    /**
     * Constructor for objects of class edu.uvm.xzhang.pokemon.Box
     * @param player
     */
    Box(Player player){
        playerId = player.getId();
        pokemons = new ArrayList<Pokemon>();
    }

    /**
     * Getter for playerId
     * @return the playerId
     */
    public int getPlayerId(){
        return playerId;
    }

    /**
     * Setter for playerId
     * @param player
     */
    public void setPlayerId(Player player){
        this.playerId = player.getId();
    }

    /**
     * Add for a pokemon
     * @param pokemon
     */
    public void addPokemon(Pokemon pokemon){
        pokemons.add(pokemon);
    }

    /**
     * Remove for a pokemon
     * @param pokemon
     * @return the removed pokemon
     */
    public Pokemon removePokemon(Pokemon pokemon){
        return pokemons.remove(pokemons.indexOf(pokemon));
    }

    /**
     * Get the certain pokemon(s) by name
     * @param name
     * @return arraylist of pokemon
     */
    public ArrayList<Pokemon> getPokemon(String name){
        ArrayList<Pokemon> certainPokemons = new ArrayList<>();
        for(Pokemon pokemon : pokemons){
            if(pokemon.getName().equals(name)){
                certainPokemons.add(pokemon);
            }
        }
        return certainPokemons;
    }

    /**
     * Get the certain pokemon by id
     * @param id
     * @return pokemon
     */
    public Pokemon getPokemon(int id){
        for(Pokemon pokemon : pokemons){
            if(pokemon.getId() == id){
                return pokemon;
            }
        }
        return null;
    }

    public int getSize(){
        return pokemons.size();
    }

    /**
     * remove all pokemons from box
     */
    public void removeAllPokemon(){
        pokemons.clear();
    }

    // toString
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(Pokemon pokemon : pokemons){
            result.append(pokemon.toString()).append("\t");
        }
        return result.toString();
    }
}
