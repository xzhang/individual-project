package edu.uvm.xzhang.pokemon;

public class Pokemon {
    // instance variables
    private String name;
    private int id;

    /**
     * Constructor for edu.uvm.xzhang.pokemon.Pokemon
     * @param name
     * @param id
     */
    Pokemon(String name, int id){
        this.name = name;
        this.id = id;
    }

    /**
     * Getter for name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Getter for id
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * setter for name
     * @return name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * setter for id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    // toString method
    @Override
    public String toString() {
        return "Pokemon{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    // equals method
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pokemon pokemon = (Pokemon) o;
        return id == pokemon.id &&
                name.equals(pokemon.name);
    }
}
