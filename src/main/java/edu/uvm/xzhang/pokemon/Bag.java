package edu.uvm.xzhang.pokemon;

import java.util.ArrayList;

public class Bag {
    // Instance variables
    private ArrayList<Pokemon> pokemons;
    private int playerId;
    private final int maxLimit;

    /**
     * Constructor
     * @param player
     */
    Bag(Player player){
        this.playerId = player.getId();
        maxLimit = 6;
        pokemons = new ArrayList<>();
    }

    /**
     * Getter for playerId
     * @return playerId
     */
    public int getPlayerId(){
        return playerId;
    }
    /**
     * Setter for pokemons
     * @param player
     */
    public void setPlayerId(Player player){
        this.playerId = player.getId();
    }


    /**
     * Getter for pokemons
     * @return size of pokemons
     */
    public int getSize(){
        return pokemons.size();
    }

    /**
     * Add pokemon to bag
     * @param pokemon
     */
    public void addPokemon(Pokemon pokemon){
        pokemons.add(pokemon);
    }

    /**
     * Check if bag is full
     * @return if bag is full
     */
    public boolean isFull(){
        return pokemons.size() >= maxLimit;
    }

    /**
     * Remove pokemon from bag
     * @return pokemon
     */
    public Pokemon removePokemon(Pokemon pokemon){
        return pokemons.remove(pokemons.indexOf(pokemon));
    }

    /**
     * Get the certain pokemon(s) by name
     * @param name
     * @return arraylist of pokemon
     */
    public ArrayList<Pokemon> getPokemon(String name){
        ArrayList<Pokemon> certainPokemons = new ArrayList<>();
        for(Pokemon pokemon : pokemons){
            if(pokemon.getName().equals(name)){
                certainPokemons.add(pokemon);
            }
        }
        return certainPokemons;
    }

    /**
     * Get the certain pokemon by id
     * @param id
     * @return pokemon
     */
    public Pokemon getPokemon(int id){
        for(Pokemon pokemon : pokemons){
            if(pokemon.getId() == id){
                return pokemon;
            }
        }
        return null;
    }

    /**
     * remove all pokemons from bag
     */
    public void removeAllPokemon(){
        pokemons.clear();
    }

    // toString
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        for(Pokemon pokemon : pokemons){
            result.append(pokemon.toString()).append("\t");
        }
        return result.toString();
    }
}
