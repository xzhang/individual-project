package edu.uvm.xzhang.pokemon;

import java.util.ArrayList;

public class Player {
    // Instance variables
    private String name;
    private int id;
    private final Box box;
    private final Bag bag;

    /**
     * constructor
     * @param name
     * @param id
     */
    public Player(String name, int id) {
        this.name = name;
        this.id = id;
        this.bag = new Bag(this);
        this.box = new Box(this);
    }


    /**
     * getter for name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * getter for id
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * getter for box
     * @return box
     */
    public Box getBox() {
        return box;
    }

    /**
     * getter for bag
     * @return bag
     */
    public Bag getBag() {
        return bag;
    }

    /**
     * setter for box
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * setter for id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
        bag.setPlayerId(this);
        box.setPlayerId(this);
    }

    /**
     * incorrect setter for id
     * @param id
     */
    public void incorrectSetId(int id) {
        this.id = id;
    }


    /**
     * add pokemon to bag;
     * if bag is full, pokemon is added to box
     * @param pokemon
     */
    public void addPokemon(Pokemon pokemon) {
        String target = "";
        if (!bag.isFull()) {
            bag.addPokemon(pokemon);
            target = "bag";
        }else{
            box.addPokemon(pokemon);
            target = "box";
        }
        System.out.println(pokemon.getName() + " was added to your " + target +" !");
    }

    /**
     * exchange pokemon from bag to box
     * @param pokemon
     */
    public void exchangePokemonFromBagToBox(Pokemon pokemon){
        box.addPokemon(bag.removePokemon(pokemon));
    }

    /**
     * exchange pokemon from box to bag
     * @param pokemon
     */
    public void exchangePokemonFromBoxToBag(Pokemon pokemon){
        bag.addPokemon(box.removePokemon(pokemon));
    }

    /**
     * Get the pokemon from bag by id
     * @param id
     * @return pokemon
     */
    public Pokemon getPokemonFromBag(int id){
        return bag.getPokemon(id);
    }
    /**
     * Get the pokemon(s) from bag by name
     * @param name
     * @return pokemon
     */
    public ArrayList<Pokemon> getPokemonFromBag(String name){
        return bag.getPokemon(name);
    }
    /**
     * Remove pokemon from bag
     * @param pokemon
     * @return pokemon
     */
    public Pokemon removePokemonFromBag(Pokemon pokemon){
        return bag.removePokemon(pokemon);
    }



    /**
     * Get the pokemon from box by id
     * @param id
     * @return pokemon
     */
    public Pokemon getPokemonFromBox(int id){
        return box.getPokemon(id);
    }
    /**
     * Get the pokemon(s) from box by name
     * @param name
     * @return pokemon
     */
    public ArrayList<Pokemon> getPokemonFromBox(String name){
        return box.getPokemon(name);
    }
    /**
     * Remove pokemon from box
     * @param pokemon
     * @return pokemon
     */
    public Pokemon removePokemonFromBox(Pokemon pokemon){
        return box.removePokemon(pokemon);
    }

    /**
     * remove all pokemons from bag
     */
    public void removeAllPokemonFromBag(){
        bag.removeAllPokemon();
    }

    /**
     * remove all pokemons from box
     */
    public void removeAllPokemonFromBox(){
        box.removeAllPokemon();
    }

    /**
     * clear bag and box
     */
    public void clearAll(){
        removeAllPokemonFromBag();
        removeAllPokemonFromBox();
    }

    // toString method
    @Override
    public String toString() {
        return "edu.uvm.xzhang.pokemon.Player: " + name + " (" + id + ")";
    }
}
